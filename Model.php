<?php
abstract class Model{
  //Declaramos variables globales
  protected $user;
  protected $pass;
  protected $db;
  protected $host;
  protected $port;
  protected $conn;
  protected static $all;
  protected $table;
  //Constructor donde asignamos el valor correspondiente a cada variable
  function __construct(){
    $this->user="root";
    $this->pass="";
    $this->db="servi-fdr";
    $this->host="127.0.0.1";
    $this->port=3306;
  }
  //Funcion donde traemos todas las filas de la tabla
  public function all(){
    return $this->executeQuery("select * from ".$this->table);
  }
  //Funcion donde traemos las filas con el campo que corresponde al valor dado
  public function where($field,$value){
    return $this->executeQuery("select * from ".$this->table." where {$field} = {$value}");
  }
  public function insert($columns,$values){
     $this->executeQuery2("INSERT INTO ".$this->table." (".$columns.") VALUES (".$values.")");
  }
  public function update($id,$precio){
     $this->executeQuery2("UPDATE ".$this->table." set precio = '".$precio."' where id = '".$id."'");
  }
  public function delete($id){
     $this->executeQuery2("DELETE FROM ".$this->table." WHERE id = '".$id."'");
  }
  public function order($nombre){
    return $this->executeQuery("select * from ".$this->table." ORDER BY (".$nombre.") ASC");
  }
  //Fu
  /*public function insert($field,$value){
    return $this->executeQuery("insert into ".$this->table. values ('null', ''));
  }*/
  //Funcion donde ejecutamos la sentencia SQL formada por otra funcion
  public function executeQuery($query){
    //Se abre la conexion;
    $this->openConnection();
    //Se obtiene el resultset de la consulta
    $result = $this->conn->query($query);
    echo $query;
    //Obtenenos todas las filas y las ponemos en data
    $data = [];
    while($row = $result->fetch_assoc()){ array_push($data,$row); }
    //Cerramos la conexion
    $this->closeConection();
    //Regresamos los datos
    return $data;
  }
  public function executeQuery2($query){
    //Se abre la conexion;
    $this->openConnection();
    //Se obtiene el resultset de la consulta
    $result = $this->conn->query($query);
    //Cerramos la conexion
    $this->closeConection();
    //Regresamos los datos
  }
  //Abrimos la conexion
  public function openConnection(){
    $this->conn = new mysqli($this->host, $this->user, $this->pass, $this->db,$this->port);
    $this->conn->set_charset('utf8');
  }
  //Cerramos la conexion
  public function closeConection(){
    mysqli_close($this->conn);
  }
}
